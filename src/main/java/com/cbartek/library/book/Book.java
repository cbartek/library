package com.cbartek.library.book;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="books")
public class Book {
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name="inc", strategy="increment")
    private Integer id;
    private String title;
    private String author;
    private boolean borrowed;

    //@OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    //private Set<Loan> loans;


    public Book() {
    }

    public Integer getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }
}
