package com.cbartek.library.book;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/book")
class BookController {
    private final Logger logger = LoggerFactory.getLogger(BookController.class);

    private BookRepository repository;

    public BookController(BookRepository repository) {
        this.repository = repository;
    }
    @GetMapping
    ResponseEntity<List<Book>> findAllBooks(){
        logger.info("Request");
        return ResponseEntity.ok(repository.findAll());
    }

    @PutMapping("/{id}")
    ResponseEntity<Book> changeStatus(@PathVariable Integer id) {
        logger.info("Change status");
        var book = repository.findById(id);
        book.ifPresent(s -> {
            s.setBorrowed(!s.isBorrowed());
            repository.save(s);
        });
        return book.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }


}
