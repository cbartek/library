package com.cbartek.library.loan;

import com.cbartek.library.book.Book;
import com.cbartek.library.user.User;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="loans")
public class Loan {
    @Id
    @GeneratedValue
    @GenericGenerator(name="inc", strategy ="increment")
    private Integer id;
    @Column(name="dateofrent")
    private Date dateOfRent;
    @Column(name="dateofreturn")
    private Date dateOfReturn;
    @ManyToOne
    @JoinColumn
    private Book book;

    @ManyToOne
    @JoinColumn
    private User user;

    public Integer getId() {
        return id;
    }

    public Date getDateOfRent() {
        return dateOfRent;
    }

    public void setDateOfRent(Date dateOfRent) {
        this.dateOfRent = dateOfRent;
    }

    public Date getDateOfReturn() {
        return dateOfReturn;
    }

    public void setDateOfReturn(Date dateOfReturn) {
        this.dateOfReturn = dateOfReturn;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
