package com.cbartek.library.loan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/loans")
public class LoanController {
    private final Logger logger = LoggerFactory.getLogger(LoanController.class);
    private LoanRepository loanRepository;

    public LoanController (LoanRepository loanRepository){
        this.loanRepository = loanRepository;
    }

    @GetMapping
    ResponseEntity<List<Object[]>> findMyOwnQueryLoan(){
        logger.info("request got");
        return ResponseEntity.ok(loanRepository.findLoansQuery());
    }

}
