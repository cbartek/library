package com.cbartek.library.loan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {

    @Query(value = "SELECT l.id, b.title, b.author, u.name, u.lastname, l.dateofrent, l.dateofreturn FROM loans l JOIN books b ON " +
            "l.book_id = b.id JOIN users u ON " +
            "l.user_id = u.id", nativeQuery = true)
    List<Object[]> findLoansQuery();
}
