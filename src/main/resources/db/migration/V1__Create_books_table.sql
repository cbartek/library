CREATE TABLE books(
id integer NOT NULL,
title char(30),
author char(30),
borrowed bit
);

insert into books(id,title,author,borrowed)
values(1,'Pan Tadeusz', 'Adam Mickiewicz', false);
