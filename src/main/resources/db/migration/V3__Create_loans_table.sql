CREATE TABLE loans(
id INTEGER NOT NULL,
dateofrent DATE,
dateofreturn DATE,
book_id INTEGER,
user_id INTEGER,
FOREIGN KEY (book_id) REFERENCES books (id),
FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO loans(id,dateofrent,dateofreturn,book_id,user_id)
values(1,'2019-11-01', '2019-11-10', 1,1)